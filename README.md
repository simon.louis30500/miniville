# BRIEF 1 • Miniville

# LIEN SITE • Miniville

https://playground.humanize.me/simon/miniville/admin.php

C'est l'histoire d'une "miniVille", celle d'une toute petite base de données SQL avec laquelle on joue, via HTML/PHP. Ça va CRUDer!

## Données fournies

- Maquette/wireframe au format draw.io
- Base de données initiale (au format SQL) composée de deux tables (habitants, users).
- Les utilisateurs du back-office sont stockés dans la table `users` :
    * utilisateur 1 :
        + user_login : guillaume@caramail.fr
        + user_password : une magnifique phrase de passe, c'est intéressant n'est-ce pas ?
        + user_role : admin
    * utilisateur 2 :
        + user_login : sarah@connor.fr
        + user_password : ma vie a basculé le jour où j'ai rencontré un T-800
        + user_role : guest

## Pages à réaliser

Dans ce brief, il est demandé de réaliser en premier lieu le BACK-OFFICE. Je répète, dans ce brief, il est demandé de réaliser en premier lieu le BACK-OFFICE. Dès que le BACK-OFFICE est terminé et fonctionnel, vous pouvez développer le FRONT.

*NB : Les pages décrites ici sont à retrouvées dans les maquettes fournies avec ce brief.*

Les pages du BACK :
- Page login : formulaire avec identifiant et mot de passe, envoyés vers la page traitement.
- Page traitement : reçoit les données de la page login. Vérifie que l'identifiant et le mot de passe correspondent à un compte existant. Vérifie également que le rôle pour cet utilisateur soit de type admin. Car seuls les admins peuvent accéder à la console d'administration.
- Page console d'administration : affiche par défaut la liste de tous les habitants de miniVille. Chaque habitant peut-être édité ou supprimé. On peut également ajouter un nouvel habitant. Lorsque qu'on édit ou ajoute un habitant, un formulaire permet de saisir Nom, Prénom, Date de naissance (au bon format), Ville, Emoji préféré (utiliser par exemple http://getemoji.com/ pour vos copier/coller)
- Une fois ce CRUD terminé, vous pouvez développer les fonctionnalités suivantes :
    + un lien "afficher tous les habitants" : retourne tous les habitants de miniVille
    + un lien "afficher les +40 ans" : retourne tous les habitants dont l'âge est au moins 40 ans

Une fois ce BACK 100% fonctionnel, vous développez le FRONT :
- page accueil (index) : affiche un habitant tiré au hasard depuis la base de données avec tous les détails (voir la maquette). L'âge affiché est calculé à partir de la date de naissance.

## Objectifs

- Réaliser un site Internet responsive, adapté aux desktop et mobile
- Importer une base de données
- Créer le lien entre le site Internet et la base de données
- Ajouter/Lire/Éditer/Supprimer des données de la base de données
- Déposer le code du projet sur un dépôt GitLab
- Publier le site Internet sur un serveur et en communiqué le lien dans le README du GitLab

## Contexte

De plus en plus amené à contribuer aux sites Internet qui permettent de gérer des données, il est nécessaire pour un développeur d’être capable de produire ce type de plateforme.

## Intension

Développer un site Internet complexe utilisant différents langages de développement web et incluant un formulaire relié avec une base de données.

## Compétences concernées

- C2: Créer une interface web statique et adaptable
- C3: Développer une interface web dynamique
- C6: Développer les composants d’accès aux données
- C7: Développer la partie back-end d’une application web ou web mobile

## Critères de réussite

- Le répertoire de la page web est structuré
- Les fichiers fournis sont structurés et commentés si besoin
- Le site utilise un langage permettant l’utilisation du SQL pour interagir avec la base de données
- Le site Internet respecte la maquette fournie
- Le site permet de créer/lire/éditer/supprimer un habitant
- Les fichiers sont déposés dans un repo GitLab
- La page web est publiée sur un serveur

## Livrables

1. Les fichiers du projet dans un dépôt GitLab
2. Le lien du site Internet du projet mentionné dans le README du dépôt GitLab contenant le projet

Les liens des livrables sont envoyés par mail aux deux formateurs :
- Jérémie : jcanavesio@simplon.co
- Géna : gpailha@simplon.co

## Deadlines

Travail individuel à livrer pour le 03/07/2020 à 16h00