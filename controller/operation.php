<?php
    $id_habitant    = $_POST["id_habitant"];
    $operation      = $_POST["operation"];

    // ces variables seront remplies uniquement si l'admin clique sur "modifier";
    $new_prenom;
    $new_nom;
    $new_naissance;
    $new_ville;
    $new_emoji;
    //////////////////////////////////////////////////////////////////////////////
    
    require("../model/pdo.php");

    $req_supprimer = $bdd->prepare("DELETE FROM habitants WHERE ppl_id = '$id_habitant'");

    $reponse;

    if($operation === "supprimer"){
        $reponse = $req_supprimer->fetch();
        $req_supprimer->execute();
        header('Location: ../admin.php');
    }
    else if($operation === "modifier"){
        $new_prenom = $_POST["modif_prenom"];
        $new_nom = $_POST["modif_nom"];
        $new_naissance = $_POST["modif_naissance"];
        $new_ville = $_POST["modif_ville"];
        $new_emoji = $_POST["modif_emoji"];
        $req_modifier = $bdd->prepare("UPDATE habitants SET ppl_prenom = '$new_prenom', ppl_nom = '$new_nom', ppl_naissance = '$new_naissance', ppl_ville = '$new_ville', ppl_emoji = '$new_emoji' WHERE ppl_id = '$id_habitant'");
        $reponse = $req_modifier->fetch();
        $req_modifier->execute();
        header('Location: ../admin.php');
    }
?>