<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/index.css">
    <title>Miniville</title>
</head>
<body>
    <?php
        require("views/header.php");
    ?>
    <br>
    <form id = "CTN_login" action="views/login.php">
        <button class = "BTN" type = "submit">Se connecter</button>
    </form>
    <br>
    <br>
    <p>Voici un habitant au hasard :</p>
    <hr>
    <section id = "section_display_random">
        <?php
            require("model/pdo.php");
            // Requete SQL pour sélectionner une ligne au hasard dans la table "habitants";
            $req_count = $bdd->query("SELECT * FROM habitants ORDER BY RAND() LIMIT 1");
    
            foreach($req_count as $row){?>
            <?php 
                $naissance = $row["ppl_naissance"];
                // Différence entre l'année actuelle et l'année de naissance;
                $age = (date('Y') - date('Y',strtotime($naissance)));
                echo "Bonjour! Je m'appelle " . $row["ppl_prenom"] . " et mon nom de famille est " . $row["ppl_nom"] . ".<br>J'ai " . $age . " ans, j'habite à " . $row["ppl_ville"] . " et voici mon émoji préféré : " . $row["ppl_emoji"];?>
        <?php }?>
    </section>
</body>
</html>