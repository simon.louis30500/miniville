<?php
require("model/pdo.php");
$req = $bdd->query("SELECT * FROM habitants");

if(isset($_POST["filtre"])){
    if($_POST["filtre"] === "40plus"){
        // je récupère la date actuelle au format Y-m-d;
        $current_year = date("Y-m-d");
        // je la convertie au format DateTime;
        $date=date_create($current_year);
        // je lui soustrais 40 années;
        date_sub($date,date_interval_create_from_date_string("40 years"));
        // je garde seulement l'année/mois/jour du résultat obtenu;
        $date_40 = date_format($date,"Y-m-d");
        $req = $bdd->query("SELECT * FROM habitants WHERE ppl_naissance < '$date_40'");
    }
    else if($_POST["filtre"] === "all"){
        $req = $bdd->query("SELECT * FROM habitants");
    }
}
// affiche tous les habitants et leurs infos dès qu'on arrive sur la page admin;
foreach($req as $row){?>
    <form class = "form_display" action = "controller/operation.php" method = "POST">
        <tr class = "form_display">
            <td><?php echo "<input type = 'text' name = 'id_habitant' value = '" . $row['ppl_id'] . "' readonly style ='border:none;cursor:default;text-align:center;'>"?></td>
            <td><?php echo $row['ppl_prenom']?></td>
            <td><?php echo $row['ppl_nom']?></td>
            <td><?php echo $row['ppl_naissance']?></td>
            <td><?php echo $row['ppl_ville']?></td>
            <td><?php echo $row['ppl_emoji']?></td>
        </tr>
        <tr class = "form_display">
            <td><?php echo "<input type = 'text' name = 'id_habitant' value = '" . $row['ppl_id'] . "' readonly style ='border:none;cursor:default;text-align:center;'>"?></td>
            <td><input type = "text" name = "modif_prenom"></td>
            <td><input type = "text" name = "modif_nom"></td>
            <td><input type = "date" name = "modif_naissance"></td>
            <td><input type = "text" name = "modif_ville"></td>
            <td><input type = "text" name = "modif_emoji"></td>
            <td><button type = "submit" name = "operation" value = "supprimer">Supprimer</button></td>
            <td><button type = "submit" name = "operation" value = "modifier">Modifier</button></td>
        </tr>
    </form>
<?php }?>