<?php
    session_start();
    unset($_SESSION["mail"]);
    unset($_SESSION["id_user"]);
    unset($_SESSION["perm"]);

    header('Location: ../views/login.php');
?>