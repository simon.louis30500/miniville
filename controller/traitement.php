<?php
    if(isset($_POST["user_login"])){
        $user_login = $_POST["user_login"];
    }
    if(isset($_POST["user_pass"])){
        $user_pass = $_POST["user_pass"];
    }
    
    require("../model/pdo.php");

    $req = $bdd->prepare("SELECT user_id, user_login, user_password, user_role FROM users WHERE user_login = '$user_login'");

    $req->execute();

    $resultat = $req->fetch();

    // Vérification du password hashed dans la DB et du password hashed renseigné dans l'input;
    $isPasswordCorrect = password_verify($user_pass, $resultat["user_password"]);
    // je récupère le role de l'utilisateur;
    $role = $resultat["user_role"];

    if(!$resultat){
        header('Location: ../views/login.php?error=Identifiant ou mot de passe invalide');
    }
    else{
        if($isPasswordCorrect) {
            session_start();
            $_SESSION['id_user'] = $resultat['user_id'];
            $_SESSION['mail'] = $user_login;
            if($role === "guest"){
                $_SESSION['perm'] = "guest";
                header("Location: ../views/login.php?error=Vous n'avez pas les bonnes permissions.");
            }
            else if($role === "admin"){
                $_SESSION['perm'] = "admin";
                header('Location: ../admin.php');
            }
        }
        else {
            header('Location: ../views/login.php?error=Identifiant ou mot de passe invalide');
        }
    }
?>