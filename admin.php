<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/admin.css">
    <title>Admin | Miniville</title>
</head>
<body>
    <?php
        require("views/header.php");
    ?>
    <?php
        session_start();
        // l'utilisateur peut accède à la page admin seulement si il est déjà connecté en tant qu'admin
        // et qu'il a les bonnes permissions;
        if(isset($_SESSION["perm"])){
            if($_SESSION["perm"] === "admin"){
            }
            else if($_SESSION["perm"] === "guest"){
                header("Location: views/login.php?error=Vous n'avez pas les bonnes permissions.");
            }
        }
        else{
            header('Location: views/login.php');
        }
    ?>
    <div id = "CTN_logout">
        <form action="controller/logout.php">
            <input class = "BTN" id = "BTN_logout" type="submit" value = "Se déconnecter">
        </form>
    </div>
    <section id = "section_admin">
        <form id = "form_filtre" action="" method="POST">
            <button class = "BTN" type="submit" name="filtre" value="40plus">Afficher habitants > 40 ans</button>
            <button class = "BTN" type="submit" name="filtre" value="all">Afficher tous les habitants</button>
        </form>
        <br>
        <table style = "text-align: center;">
        <form action="controller/ajouter.php" method = "POST">
            <td><button type = "submit">Ajouter</button></td>
            <td><input type = "text" name = "add_prenom"></input></td>
            <td><input type = "text" name = "add_nom"></input></td>
            <td><input type = "date" name = "add_naissance"></input></td>
            <td><input type = "text" name = "add_ville"></input></td>
            <td><input type = "text" name = "add_emoji"></input></td>
        </form>
        <tr>
            <!-- titre colonnes du tableau -->
            <th><strong>ID</strong></th>
            <th><strong>Prénom</strong></th>
            <th><strong>Nom</strong></th>
            <th><strong>Naissance</strong></th>
            <th><strong>Ville</strong></th>
            <th><strong>Emoji</strong></th>
        </tr>
        <?php
            require("views/all_data.php");
        ?>
        </table>
    </section>
</body>
<script>
    document.getElementById("titre_header").addEventListener("click", function(){
        window.location = "index.php";
    })
</script>
</html>