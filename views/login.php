<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/common.css">
    <link rel="stylesheet" href="../assets/css/login.css">
    <title>Login | Miniville</title>
</head>
<body>
    <?php
        require("header.php");
    ?>
    <?php
        if(isset($_GET["error"])){
            echo "<p id = 'error'>" . $_GET["error"] . " " . "&#128577</p>";
        }
    ?>
    <br><br>
    <section id = "section_login">
        <form id = "form_login" action="../controller/traitement.php" method = "POST">
            <label>Identifiant</label>
            <input type = "text" name = "user_login">
            <label>Mot de passe</label>
            <input type = "password" name = "user_pass">
            <input class = "BTN" id = "BTN_login" type="submit" value = "Se connecter">
        </form>
    </section>
</body>
<script>
    document.getElementById("titre_header").addEventListener("click", function(){
        window.location = "../index.php";
    })
</script>
</html>