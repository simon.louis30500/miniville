-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 02 juil. 2020 à 11:32
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `brief1-bdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `habitants`
--

DROP TABLE IF EXISTS `habitants`;
CREATE TABLE IF NOT EXISTS `habitants` (
  `ppl_id` int(11) NOT NULL AUTO_INCREMENT,
  `ppl_nom` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `ppl_prenom` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `ppl_naissance` date DEFAULT NULL,
  `ppl_ville` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ppl_emoji` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`ppl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `habitants`
--

INSERT INTO `habitants` (`ppl_id`, `ppl_nom`, `ppl_prenom`, `ppl_naissance`, `ppl_ville`, `ppl_emoji`) VALUES
(1, 'Martin', 'Patrice', '1980-12-12', 'Cendras', '🦊'),
(2, 'Petit', 'Camélia', '1973-03-01', 'Saint-Paul-la-Coste', '🌻'),
(3, 'Richard', 'Jean', '1944-03-03', 'Soustelle', '🍄'),
(4, 'Durand', 'Rener', '1989-10-04', 'Saint-Paul-la-Coste', '🦊'),
(5, 'Dubois', 'Michel', '1976-03-05', 'Cendras', '🐶'),
(6, 'Girard', 'André', '1981-08-26', 'Corbès', '🌻'),
(7, 'Lefebre', 'Louis', '1981-05-23', 'La Grand-Combe', '🐿'),
(8, 'Morel', 'Alain', '1974-05-20', 'Saint-Paul-la-Coste', '🌼'),
(9, 'Mercier', 'Marcel', '2000-11-01', 'Saint-Paul-la-Coste', '🐼'),
(10, 'Boyer', 'Daniel', '1965-12-03', 'La Grand-Combe', '🤓'),
(11, 'Faure', 'Pierre', '1974-07-05', 'Corbès', '🌹'),
(12, 'Guerin', 'Claude', '1991-01-06', 'Anduze', '🐅'),
(13, 'Perrin', 'Marie', '1985-01-31', 'Cendras', '🍉'),
(14, 'Masson', 'Françoise', '1976-09-30', 'Vézénobres', '🍔'),
(15, 'Garcia', 'Nicole', '1954-03-23', 'Montpellier', '🏀'),
(16, 'Dumont', 'Marguerite', '2010-01-01', 'Saint-Paul-la-Coste', '🐼'),
(17, 'Roche', 'Denise', '1943-01-23', 'Paris', '🍷'),
(18, 'Leroux', 'Gérard', '1986-08-01', 'Saint-Paul-la-Coste', '🦏'),
(19, 'Barbier', 'Paul', '1980-03-24', 'Anduze', '🍔'),
(20, 'Joly', 'Christiane', '1990-03-19', 'Cendras', '🌹'),
(21, 'Aubert', 'Sophie', '1999-04-28', 'Saint-Paul-la-Coste', '🍺');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
